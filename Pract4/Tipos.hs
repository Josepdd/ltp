module Tipos where

type Person = String
type Book = String
type Database = [(Person,Book)]

exampleBase :: Database
exampleBase = [("Alicia", "El nombre de la rosa"), ("Juan","La hija del canibal"), ("Pepe", "Odesa"), ("Alicia", "La ciudad de las bestias")]

obtain :: Database -> Person -> [Book]
obtain dBase thisperson = [book | (person,book) <- dBase, person == thisperson ]

borrow :: Database -> Book -> Person -> Database
borrow dBase thisp thisb = (thisp, thisb):dBase

return' :: Database -> Book -> Person -> Database
return' dBase thisp thisb = [(person,book) | (person,book)<-dBase, (person,book) /= (thisp, thisb)]

