module ISort where

iSort :: [Int] -> [Int]
iSort [] = [] 
iSort (x:xs) = ins x (iSort xs)

ins :: Int -> [Int] -> [Int]
ins x xs = (filter (< x) xs) ++ [x] ++ (filter (>= x) xs)
