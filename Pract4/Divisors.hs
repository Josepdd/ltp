module Divisors where

-- Función que devuelve una lista con los divisores de un numero
divisors :: Int -> [Int]
divisors x = [y | y <- [1..x], x `mod` y == 0]

divisors2 :: Int -> [Int]
divisors2 n = filter((0 ==) . (n `mod`)) [1 .. n] 

divisors3 :: Int -> [Int]
divisors3 x = divisoresAUX x 1

divisoresAUX :: Int -> Int -> [Int]
divisoresAUX num contador
	| num == contador         = [contador]
	| (mod num contador) == 0 = [contador] ++ divisoresAUX num (contador+1)
	| (mod num contador) /= 0 = 			  divisoresAUX num (contador+1)

-- Función que devuelve si un numero es primo o no
isPrime :: Int -> Bool
isPrime x
    | x == 1 = True
    | (length xs) == 2 = True
    | otherwise = False
        where xs = divisors3 x

-- Función que devuelve los x primeros primos
primes :: Int -> [Int]
primes x = take x (filter (isPrime) [1..])