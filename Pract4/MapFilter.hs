module MapFilter where

filter' :: (a -> Bool) -> [a] -> [a]
filter' p xs = [x | x<-xs, p x]

map' :: (a -> a) -> [a] -> [a]
map' p xs = [p x | x<-xs]

-- Funcion auxiliar para que funcione la funcion (sum . map square . filter even) [1..10]
square :: Int -> Int
square x = x*x