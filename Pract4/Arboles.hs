module Arboles where

data Tree a = Leaf a | Branch (Tree a) (Tree a) deriving Show

data BinTreeInt = Void | Node Int BinTreeInt BinTreeInt deriving Show

data Tree2 a = Branch2 a (Tree2 a) (Tree2 a) | Void2 deriving Show

-- Usando el tipo Tree
numleaves :: Tree a -> Int
numleaves (Leaf x) = 1
numleaves (Branch a b) = numleaves a + numleaves b

symmetric :: Tree a -> Tree a
symmetric (Leaf x) = (Leaf x)
symmetric (Branch a b) = (Branch (symmetric b) (symmetric a))

listToTree :: [a] -> Tree a
listToTree (x:xs)
	| length xs > 0  = (Branch (Leaf x) (listToTree xs))
	| length xs == 0 = (Leaf x)

listToTree' :: [a] -> Tree a
listToTree' (x:xs)
	| length xs > 0  = (Branch (listToTree' (fst(hxs)))(listToTree' (snd(hxs))))
	| length xs == 0 = (Leaf x)
		where hxs = half (x:xs)

half :: [a] -> ([a], [a])
half [] = ([],[])
half [x] = ([x], [])
half (x:y:xys) = (x:xs, y:ys) where (xs, ys) = half xys

listToTree'' :: [a] -> Tree a
listToTree'' [x] =  (Leaf x)
listToTree'' xs = (Branch (listToTree'' half1) (listToTree'' half2))
	where
		half = (div (length xs) 2)
		half1 = take half xs
		half2 = drop half xs

treeToList :: Tree a -> [a]
treeToList (Leaf x) = [x]
treeToList (Branch a b) = (treeToList a) ++ (treeToList b)

-- Usando el tipo BinTreeInt
insTree :: Int -> BinTreeInt -> BinTreeInt
insTree i Void = Node i Void Void
insTree i (Node a b c)
	| i <= a    = Node a (insTree i b) c
	| otherwise = Node a b (insTree i c)

creaTree :: [Int] -> BinTreeInt
creaTree [] = Void
creaTree (x:xs) = insTree x (creaTree xs)

treeElem :: Int -> BinTreeInt -> Bool
treeElem x Void = False
treeElem x (Node a b c)
	| x == a = True
	| otherwise = or [treeElem x b, treeElem x c]

dupElem :: BinTreeInt -> BinTreeInt
dupElem Void = Void
dupElem (Node a b c) = (Node (2*a) (dupElem b) (dupElem c))

countProperty :: (a -> Bool) -> (Tree2 a) -> Int
countProperty f Void2 = 0
countProperty f (Branch2 a b c)
	| f a == True = 1 + (countProperty f b) + (countProperty f c)
	| f a == False = (countProperty f b) + (countProperty f c)