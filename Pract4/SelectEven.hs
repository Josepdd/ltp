module SelectEven where

-- Funcion que devuelve una lista con los numero pares de otra lista
selectEven :: [Int] -> [Int]
selectEven xs = [x | x<-xs, even x]

selectEven' :: [Int] -> [Int]
selectEven' xs = filter (even) xs

-- Funcion que devuelve una lista con las posiciones de los numeros pares en otra lista
selectEvenPos :: [Int] -> [Int]
selectEvenPos xs = [ y | y<-[0..length xs-1], even (xs !! y) ]

selectEvenPos' :: [Int] -> [Int]
selectEvenPos' xs = auxEvenPos xs 0

auxEvenPos :: [Int] -> Int -> [Int]
auxEvenPos xs x
	| x >= length xs 	  = []
	| even (xs !! x) 	  = x:(auxEvenPos xs (x+1))
	| not(even (xs !! x)) = auxEvenPos xs (x+1)