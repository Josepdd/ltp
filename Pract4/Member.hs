module Member where

member :: Int -> [Int] -> Bool
member x xs = if x `elem` xs then True else False