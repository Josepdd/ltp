module Shape where

	type Height = Float
	type Width = Float
	type Radius = Float

	data Rectangle = Rectangle Height Width -- deriving (Eq, Show)
	data Circle = Circle Radius -- deriving (Eq, Show)

	class (Eq a, Show a) => Shape a where
		area :: a -> Float
		perimeter :: a -> Float

	instance Eq Circle where
		(Circle r) == (Circle a) = r==a

	instance Eq Rectangle where
		(Rectangle x y) == (Rectangle a b) = x==a && y==b

	instance Show Circle where
		show (Circle a) = "Circulo con radio: " ++ show a

	instance Show Rectangle where
		show (Rectangle x y) = "Rectangulo con base: " ++ show x ++ " y altura: " ++ show y 

	instance Shape Rectangle where
		area (Rectangle h w) = h * w
		perimeter (Rectangle h w) = (h+w)*2

	instance Shape Circle where
		area (Circle r) = pi * r**2
		perimeter (Circle r) = 2*pi*r

	type Volume = Float
	type Surface = Float

	volumePrism :: (Shape a) => a -> Height -> Volume
	volumePrism base height = (area base) * height

	surfacePrism :: (Shape a) => a -> Height -> Surface
	surfacePrism base height = ((perimeter base) * height) + 2*(area base) 

	