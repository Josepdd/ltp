data Nat = Zero | Suc Nat deriving (Eq, Ord)

(+) :: (Nat a) => a -> a -> a
(+) a b = a+b
